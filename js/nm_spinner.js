'use strict'


var canvas = document.getElementById('ag_main');
var $canvas = jQuery(canvas);

var cw = canvas.width;
var ch = canvas.height;
var ctx = canvas.getContext('2d');

ctx.font = '16px monospace';

var pix = ctx.createImageData(1,1); // only do this once per page
var d  = pix.data;                        // only do this once per page
d[0]   = Math.floor(Math.random() * 256);
d[1]   = Math.floor(Math.random() * 256);
d[2]   = Math.floor(Math.random() * 256);
d[3] = 255

var spinImage = new Image(300, 300);
spinImage.src = '/wp-content/themes/marty_party/images/spinner/spinner.png';
spinImage.style = "display: none";
document.body.appendChild(spinImage)

function getMousePos(canvas, evt) {
	var rect = canvas.getBoundingClientRect();
	return {
		x: (evt.clientX - rect.left) * (cw/canvas.offsetWidth),
		y: (evt.clientY - rect.top) * (cw/canvas.offsetWidth)
	};
}

function rotate(cx, cy, x, y, angle) {
    var radians = (Math.PI / 180) * angle,
        cos = Math.cos(radians),
        sin = Math.sin(radians),
        nx = (cos * (x - cx)) + (sin * (y - cy)) + cx,
        ny = (cos * (y - cy)) - (sin * (x - cx)) + cy;
    return {x: nx, y: ny};
}

/* RENDER SPINNER */

//half canvas width + half image width
var spinner_x = (cw / 2) - (spinImage.width / 2);
var spinner_y = (ch / 2) - (spinImage.height / 2);

setTimeout(function(){
	
}, 10)
	

setInterval(draw_frame, 16);

//Hi droke
var drokepoints = 0;
var $drokeover = jQuery(".droke_over");

var currAngle = 0;
var spinrate = 0;
function draw_frame()
{
	ctx.clearRect(0,0, cw, ch);


	ctx.save();
    ctx.translate(canvas.width/2,canvas.height/2);

    ctx.rotate(currAngle);

   	ctx.drawImage( spinImage, -(spinImage.width / 2), -(spinImage.height / 2)-22 );
    ctx.restore();

    if (!mouseDown)
    {
    	currAngle += spinrate;
		currAngle %= (2*Math.PI);

		spinrate = spinrate * 0.97;
		drokepoints += Math.abs(spinrate);
    }

    drokepoints -= 0.1;
    drokepoints = Math.max(0, drokepoints);
    drokepoints = Math.min(5, drokepoints);

    if (drokepoints > 1)
    {
    	var opacity = parseFloat($drokeover.css("opacity"));
    	opacity += 0.001 * drokepoints;
    	opacity = Math.min(0.9, opacity);
    	$drokeover.css("opacity", opacity);
    }
    else
    {
    	var opacity = parseFloat($drokeover.css("opacity"));
    	opacity -= 0.01;
    	opacity = Math.max(0.0, opacity);
    	$drokeover.css("opacity", opacity);
    }
    
}

//===============================
//MOUSE STUFF
//===============================
var mouseStart = {x :0, y: 0};
var mouseEnd  = {x :0, y: 0};
var lastAngle = 0.0;

var touchAngle = 0;

var spinCW = true;

var mouseDown = false;

$canvas.on('mousedown touchstart', function(event)
{
	event.preventDefault();
	mouseDown = true;
	var mouseCurr = getMousePos(canvas, event);

	touchAngle = Math.atan2((mouseCurr.x - (cw / 2)), ( (ch / 2) - mouseCurr.y));
	touchAngle = touchAngle - currAngle;

	mouseStart = mouseCurr;
	lastAngle = currAngle;
});

$canvas.on('mousemove touchmove', function(event)
{
	event.preventDefault();
	if(mouseDown) 
	{
		var mouseCurr = getMousePos(canvas, event);

		lastAngle = currAngle;

		var newAngle = Math.atan2((mouseCurr.x - (cw / 2)), ( (ch / 2) - mouseCurr.y));
		currAngle =   (newAngle - touchAngle);		

		mouseEnd = mouseCurr;
	}
});

jQuery(window.document).on('mouseup touchend', function(event)
{
	mouseDown = false;
	spinrate = (currAngle - lastAngle)*7;
	spinrate = Math.min(spinrate,  .5);
	spinrate = Math.max(spinrate, -.5)
});

/*
//===============================
//MOUSE STUFF
//===============================
var mouseStart = {x :0, y: 0};

var mouseDown = false;
$canvas.on('mousedown touchstart', function(event)
{
	event.preventDefault();
	mouseDown = true;

	mouseStart = getMousePos(canvas, event);
});

$canvas.on('mousemove touchmove', function(event)
{
	event.preventDefault();
	if(mouseDown) 
	{
		var mousePos = getMousePos(canvas, event);

		ctx.strokeStyle = 'black';
		ctx.clearRect(0,0, cw, ch);
		ctx.beginPath();
		ctx.lineWidth=2;

		ctx.moveTo(mouseStart.x, mouseStart.y);
		ctx.lineTo(mousePos.x, mousePos.y);

		ctx.stroke();		
	}
});

jQuery(window.document).on('mouseup touchend', function(event)
{

	ctx.clearRect(0,0, cw, ch);
	mouseDown = false;
});
*/
