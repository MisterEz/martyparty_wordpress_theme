<?php
/**
 * Header Template
 *
 * @see http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Scaffolding
 * @since Scaffolding 1.0
 *
 */ ?><!DOCTYPE html>
<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

<!--[if lt IE 9]>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv-printshiv.min.js"></script>
<![endif]-->

<?php 
	wp_head(); 
?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-72800577-1', 'auto');
  ga('send', 'pageview');
</script>

</head>

<body <?php body_class(); ?>>
	<div id="container">
	
		<header id="masthead" class="header" role="banner">
			

			<nav id="main-navigation" class="clearfix navbar_over" role="navigation" aria-label="<?php _e( 'Primary Navigation', 'scaffolding' ); ?>">

				<div class="container-fluid">
					
					<a class="navbar-brand" href="<?= get_site_url() ?>"><img src='<?= get_stylesheet_directory_uri() ?>/images/mplogo.png' alt='Marty Party'></a>
				
					<?php scaffolding_main_nav(); ?>

				</div>

			</nav>

		</header>

		<div class="cover_image mp_cover_<?= mp()->mp_cover_size ?>" style="background-image: url(<?= mp()->mp_cover_image_url ?>);">
	        <div class="cover-dark slide-bg">
	        </div>

	        <div class='header-gradient'>
	    	</div>
	    	<div class="header_offset">
	        	<div class="container-fluid">
		            <div class='cover_image_center'>

		            	<?php

			                if (mp()->mp_cover_html_on && mp()->mp_cover_html_location == "before")
			                {
			                	echo mp()->mp_cover_html;
			                }

			           

			                if (mp()->mp_cover_html_on && mp()->mp_cover_html_location == "replace")
			                {
			                	echo mp()->mp_cover_html;
			                }
			                else
			                {
			                	?>

			             <h1 class=''><?= mp()->mp_title ?></h1>
		               	 <div class='cover_subtitle'><?= mp()->mp_subtitle ?></div>

			                	<?php
			                }

			                if (mp()->mp_cover_html_on && mp()->mp_cover_html_location == "after")
			                {
			                	echo mp()->mp_cover_html;
			                }

			            ?>
		            </div>
	            </div>
	        </div>
	        
	    </div>
			
	
		<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'scaffolding' ); ?></a>
		<div id="content">

			<div id="inner-content" class="clearfix">

				<?php // Test for active sidebars to set the main content width
					if ( is_active_sidebar( 'left-sidebar' ) && is_active_sidebar( 'right-sidebar' ) ) { // both sidebars
						$main_class = 'col-sm-6 col-sm-push-3';
					} elseif ( is_active_sidebar( 'left-sidebar' ) && ! is_active_sidebar( 'right-sidebar' ) ) { // left sidebar
						$main_class = 'col-sm-9 col-sm-push-3';
					} elseif ( ! is_active_sidebar( 'left-sidebar' ) && is_active_sidebar( 'right-sidebar' ) ) { // right sidebar
						$main_class = 'col-sm-9';
					} else { // no sidebar
						$main_class = 'col-xs-12';
					}
				?>

				<div id="main" class="<?php echo $main_class; ?> clearfix" role="main">
