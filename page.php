<?php
/**
 * Default Page Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Scaffolding
 * @since Scaffolding 1.0
 */

get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

				<?php /*<header class="page-header">

					<h1 class="page-title"><?php the_title(); ?></h1>

				</header> */ ?>

				<section class="page-content clearfix">

					<?php 
				if (mp() !== null && !post_password_required( get_post() ))
					{
						if (mp()->mp_html_on && mp()->mp_html_location ==="before")
						{
							echo mp()->mp_html;
						}

						if (mp()->mp_html_on && mp()->mp_html_location ==="replace")
						{
							echo mp()->mp_html;
						}
						else
						{
							echo "
							<div class='wrap'>";
								the_content();
							echo "
							</div>";
						}

						if (mp()->mp_html_on && mp()->mp_html_location ==="after")
						{
							echo mp()->mp_html;
						}
					}
					else
					{
						echo "
						<div class='wrap'>";
							the_content();
						echo "
						</div>";
					}

					?>

					<?php wp_link_pages( array(
							'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'scaffolding' ) . '</span>',
							'after'       => '</div>',
							'link_before' => '<span>',
							'link_after'  => '</span>',
					) ); ?>

				 </section>

				<?php
					  // If comments are open or we have at least one comment, load up the comment template
					  if ( comments_open() || '0' != get_comments_number() ) :
							comments_template();
					  endif;
				 ?>

			</article>

		<?php endwhile; ?>

	<?php else : ?>

		<?php get_template_part( 'template-parts/error' ); // WordPress template error message ?>

	<?php endif; ?>

<?php get_footer();
