<?php if (!defined('ABSPATH')){die();};

function members_only( $atts ){
	// There is no actual "members only" functionality. 
	// but this displays a _fake_ message asking the use to login
	// or register

	return '
	<div class="members_only text-center">
		<h2>Sorry!</h2>
		<h3>This page is for members only.</h3>
		<p>Forget to sign in? <a href="/login">Login here</a></p>

		<br>

		<img class="aligncenter size-medium" src="/wp-content/uploads/2017/04/21-guilty-dog-620x413.jpg" alt="Sorry Dog" width="300" height="200" />

		<br>
		<p>Want to see what you\'re missing out on? </p>
		<a class="btn btn-primary btn-lg" href="/register">Register</a>


	</div>

	';
}
add_shortcode( 'members_only', 'members_only' );



// Shortcodes actually run too late to change the header
// so we look ahead in post_content here
//
// NOTE: look aheads won't run if shortcode not
// called from post_content (i,e from a widget)
//
function look_ahead_member_only()
{
	$this_id = get_the_ID(); 
	$this_content = get_post_field( 'post_content', $this_id );

	if ( has_shortcode( $this_content, 'members_only' ) )
	{
		//Send a 401 (for google i guess)
		status_header(401);
	}
	
}
add_action( 'template_redirect', 'look_ahead_member_only' );
