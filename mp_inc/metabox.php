<?php if (!defined('ABSPATH')){die();};
add_filter( 'rwmb_meta_boxes', 'mp_page_options' );
function mp_page_options( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => 'Page Options',
        'post_types' => array('page', 'post'),
        'context'    => 'side',
        'priority'   => 'low',
        'fields'     => array(
            array(
                'id'   => 'mp_title',
                'name' => 'Page Title',
                'type' => 'text',
            ),
            array(
                'id'   => 'mp_subtitle',
                'name' => 'Page Subtitle',
                'type' => 'text',
            ),
            array(
                'id'               => 'mp_cover_image',
                'name'             => 'Cover Image - <300kb',
                'desc'             => 'something like 1600x400, compressed jpeg, progressive. <br> <small>i will <i>face fuck</i> u if it\'s >=300kb</small>',
                'type'             => 'image_advanced',
                'force_delete'     => false,
                'class'            => 'mp_media_input',
            ),
            array(
                'id'   => 'mp_cover_size',
                'name' => 'Cover Image size',
                'type' => 'radio',

                'std'  => 'small',
                'options' => array(
                    'full'  => "The whole scren",
                    'med'   => "Like Half",
                    'small' => "Fun Sized (Default)",

                ),
            ),
            
        ),
    );
    return $meta_boxes;
}

add_filter( 'rwmb_meta_boxes', 'mp_advanced' );
function mp_advanced( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => 'Nathan Mitchell\'s Advanced Options <small style="color: #aaa">(For big boys who aren\'t stupid)</small>',
        'post_types' => array('page', 'post'),
        'context'    => 'advanced',
        'default_hidden' => 'true',
        'priority'   => 'high',
        'fields'     => array(
            array(
                'id'   => 'html_sep',
                'name' => '',
                'type' => 'custom_html',
                'std'  => '<hr><br><h3>Adv. HTML</h3>'           
            ), 
            array(
                'id'   => 'mp_html_on',
                'name' => 'Enable HTML override?',
                'type' => 'checkbox',
                'std'  => '0'           
            ),
            array(
                'id'   => 'mp_html_location',
                'name' => 'HTML location',
                'type' => 'radio',

                'std'  => 'before',
                'options' => array(
                    'before' => "Before Post",
                    'after' => "After Post",
                    'replace' => "Replace Post",

                ),
            ),
            array(
                'id'   => 'mp_html',
                'name' => 'Page HTML',
                'type' => 'wysiwyg',
                'raw'  => true,
                'options' => array(
                    'tinymce' => false,
                    'teeny' => true
                    )
            ),
            array(
                'id'   => 'header_sep',
                'name' => '',
                'type' => 'custom_html',
                'std'  => '<hr><br><h3>Adv. Header &amp; Footer</h3>'           
            ), 
            array(
                'id'   => 'mp_head_html_on',
                'name' => 'Include header & footer HTML?',
                'type' => 'checkbox',
                'std'  => '0'           
            ),
            array(
                'id'   => 'mp_head_html',
                'name' => 'Header HTML',
                'type' => 'wysiwyg',
                'raw'  => true,
                'options' => array(
                    'tinymce' => false,
                    'teeny' => true,
                    'textarea_rows' => 3
                    )
            ), 
            array(
                'id'   => 'mp_foot_html',
                'name' => 'Footer HTML',
                'type' => 'wysiwyg',
                'raw'  => true,
                'options' => array(
                    'tinymce' => false,
                    'teeny' => true,
                    'textarea_rows' => 3
                    )
            ), 
            array(
                'id'   => 'cover_sep',
                'name' => '',
                'type' => 'custom_html',
                'std'  => '<hr><br><h3>Adv. Cover Options</h3>'           
            ),
            array(
                'id'   => 'mp_cover_html_on',
                'name' => 'Include cover HTML?',
                'type' => 'checkbox',
                'std'  => '0'           
            ),
            array(
                'id'   => 'mp_cover_html_location',
                'name' => 'Cover HTML location',
                'type' => 'radio',

                'std'  => 'before',
                'options' => array(
                    'before' => "Before Post",
                    'after' => "After Post",
                    'replace' => "Replace Post",

                ),
            ),
            array(
                'id'   => 'mp_cover_html',
                'name' => 'Cover HTML',
                'type' => 'wysiwyg',
                'raw'  => true,
                'options' => array(
                    'tinymce' => false,
                    'teeny' => true,
                    'textarea_rows' => 3
                    )
            ),
        ),
    );
    return $meta_boxes;
}
