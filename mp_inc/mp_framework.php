<?php if (!defined('ABSPATH')){die();};
final class mp_framework
{

	protected static $_instance = null;


	//Make new object for page shit
	public $default_cover;

	//Meta

	//Cover stuff
	public $mp_title;
	public $mp_subtitle;

	public $mp_cover_image;
	public $mp_cover_image_url;

	//Nathan Mitchell's Advanced bullshit
	public $mp_html_on;
	public $mp_html_location;
	public $mp_html;

	public $mp_head_html_on;
	public $mp_head_html;
	public $mp_foot_html;

	public $mp_cover_html_on;
	public $mp_cover_html_location;
	public $mp_cover_size;
	public $mp_cover_html;

	function __construct() {
		$this->default_cover = get_stylesheet_directory_uri() . '/images/frontpage/martysafaricover.jpg';

		//Cover stuff
		$this->mp_title       = (rwmb_meta('mp_title') !== '')       ? esc_html(rwmb_meta('mp_title'))       : get_the_title();
		$this->mp_subtitle    = (rwmb_meta('mp_subtitle') !== '')    ? esc_html(rwmb_meta('mp_subtitle'))    : '';

		$this->mp_cover_image = (rwmb_meta('mp_cover_image') !== '') ? rwmb_meta('mp_cover_image','size=full&amp;limit=1') : null; 

		$this->mp_cover_image_url = ($this->mp_cover_image != null) ? reset($this->mp_cover_image)['full_url'] : $this->default_cover;

		//Nathan Mitchell's Advanced bullshit
		$this->mp_html_on       = (rwmb_meta('mp_html_on') !== '')          ? rwmb_meta('mp_html_on')     : null;
		$this->mp_html_location = (rwmb_meta('mp_html_location') !== '')    ? rwmb_meta('mp_html_location')    : null;
		$this->mp_html          = (rwmb_meta('mp_html') !== '')             ? rwmb_meta('mp_html') : null;

		$this->mp_head_html_on  = (rwmb_meta('mp_head_html_on') !== '')     ? rwmb_meta('mp_head_html_on') : null;
		$this->mp_head_html     = (rwmb_meta('mp_head_html') !== '')        ? rwmb_meta('mp_head_html') : null;
		$this->mp_foot_html     = (rwmb_meta('mp_foot_html') !== '')        ? rwmb_meta('mp_foot_html') : null;

		$this->mp_cover_html_on  = (rwmb_meta('mp_cover_html_on') === '1');
		$this->mp_cover_html_location = (rwmb_meta('mp_cover_html_location') != null)    ? rwmb_meta('mp_cover_html_location')    : null;
		$this->mp_cover_size = (rwmb_meta('mp_cover_size') !== '')        ? rwmb_meta('mp_cover_size') : 'small';
		$this->mp_cover_html     = (rwmb_meta('mp_cover_html') !== '')        ? rwmb_meta('mp_cover_html') : null;

	}
	public static function instance() {
		if ( is_null ( self::$_instance ) )
		{
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	public function __clone() 
	{
		die();
	}

	
}
function mp()
{
	return mp_framework::instance();
}


//Enqueue head and footer
function mp_nmab_head()
{
	echo mp()->mp_head_html;
}
function mp_nmab_footer()
{
	echo mp()->mp_foot_html;
}

if (mp()->mp_head_html_on == true)
{
	add_action('wp_head', 'mp_nmab_head');
	add_action('wp_footer', 'mp_nmab_footer');
}

?>
